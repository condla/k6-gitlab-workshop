# Automated k6 load testing with Gitlab CI/CD

This repository provides an example of how to set up [Grafana k6](https://github.com/grafana/k6) with [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/) to add reliability testing into an automation flow.

## Introduction

In this workshop, we test https://quickpizza.eyeveebee.net/. [QuickPizza](https://github.com/grafana/quickpizza) is a web demo that generates new and exciting pizza combinations.

The repository demonstrates various tests using GitLab CI/CD:

- [API load testing](https://k6.io/docs/testing-guides/api-load-testing/).
- [End-to-end web testing](https://k6.io/docs/using-k6-browser/overview/).
- [Combined tests](https://k6.io/docs/testing-guides/load-testing-websites/#hybrid-load-testing) (API and end-to-end).
- Visualize test results in [Grafana Cloud k6](https://grafana.com/docs/grafana-cloud/k6/).

## Pre-requisites 

The only requirement is an account on [https://gitlab.com](https://gitlab.com) to fork this repository.

## Getting Started

- [Fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) this repository. 

    ![Fork the repository](./images/fork.png)

## Run an API Test

- In this step, you'll execute the script [scripts/api.js](./scripts/api.js) in a test stage. It's a basic [HTTP API test](https://k6.io/docs/testing-guides/api-load-testing/) on https://quickpizza.eyeveebee.net/.
- Go to **your forked repository** on gitlab.com and edit the file [`.gitlab-ci.yml`](.gitlab-ci.yml). 

    ![Edit pipeline](./images/open-web-ide.png)

- Add the following content:

    ```yaml
    stages:
    - api-test

    api-test:
      image:
        name: grafana/k6:0.46.0
        entrypoint: [""]
      stage: api-test
      script:
        - echo "executing k6 api test"
        - k6 run ./scripts/api.js
    ```

    ![Edit pipeline](./images/edit-pipeline.png)

- Commit the changes.

  ![Edit pipeline](./images/commit-changes.png)

- Select on the left menu "Build" and "Jobs".

  ![Gitlab Jobs](./images/gitlab-jobs.png)

- Click on the job to view the results of running [scripts/api.js](./scripts/api.js).

  ![API test results](./images/api-test-results.png)

### Run an API Test that fails

Now you'll see how a failed test can make the CI/CD job fail. Using [thresholds](https://docs.k6.io/docs/thresholds) you'll configure your Service Level Objectives (SLOs) as `pass/fail` criteria.

- Edit the file [scripts/api.js](./scripts/api.js). You can use the Web IDE as we did in the previous steps, or the `Edit a single file` option.

  ![Edit single file](./images/edit-test-api.png)

- Replace the threshold, `http_req_duration` (one of the [HTTP-specific built-in metrics](https://k6.io/docs/using-k6/metrics/reference/#http)), with a lower value (e.g. `200`):

    ```javascript
    thresholds: {
        http_req_duration: ['p(95)<200'],
    },
    ```
- Commit the file changes. This will re-run the pipeline with the changed test.
- If a threshold in your test fails, k6 will finish with a non-zero exit code, which communicates to GitLab that the step failed. 
    - We want the test to fail if the 95th percentile response time for the http request duratioon is below 200ms.
- Select on the left menu "Build" and "Jobs". There should be a failed job:

  ![Failed pipeline](./images/failed-pipeline-threshold.png)

- Click on the job to see the details. Scroll down and you can see the reason why it failed:

    ```
    time="2023-08-15T17:57:27Z" level=error msg="thresholds on metrics 'http_req_duration' have been crossed"
    Cleaning up project directory and file based variables
    00:00
    ERROR: Job failed: exit code 99
    ```

    ![Failed pipeline details](./images/failed-pipeline-threshold-detail.png)

## Run and end-to-end test

- In this step, you'll execute the script [scripts/broser.js](./scripts/browser.js) in a test stage. It's a basic [browser test](https://k6.io/docs/using-k6-browser/overview/) on https://quickpizza.eyeveebee.net/.
- Go to **your forked repository** on gitlab.com and edit the file [`.gitlab-ci.yml`](.gitlab-ci.yml). Add the following content:

    ```yaml
    stages:
    - browser-test

    browser-test:
      image:
        name: grafana/k6:0.46.0-with-browser
        entrypoint: [""]
      stage: browser-test
      script:
        - echo "executing k6 browser test"
        - k6 run ./scripts/browser.js
    ```

- The Docker image is now `grafana/k6:0.46.0-with-browser`. It is a k6 Docker image that includes Chromium, which is necessary to execute browser tests. 
    - See https://hub.docker.com/r/grafana/k6 for more details on the available docker images.
- Commit the changes.

- Select on the left menu "Build" and "Jobs".

  ![Job List](./images/browser-test-results.png)

- Click on the job to view the results of running [scripts/browser.js](./scripts/browser.js). Review the [browser metrics output](https://k6.io/docs/using-k6-browser/browser-metrics/#understanding-the-browser-metrics-output).

  ![Browser test results](./images/browser-test-results-details.png)

## Run a combined test (API + Browser)

- In this step, you'll execute the script [scripts/composability.js](./scripts/composability.js) in a test stage. It's what we call a hybrid or combined test on https://quickpizza.eyeveebee.net/, which runs:
    - A [smoke test](https://k6.io/docs/test-types/smoke-testing/).
    - An [API test](https://k6.io/docs/testing-guides/api-load-testing/).
    - An [end-2-end (browser) test](https://k6.io/docs/using-k6-browser/overview/).
- Go to **your forked repository** on gitlab.com and edit the file [`.gitlab-ci.yml`](.gitlab-ci.yml). Add the following content:

    ```yaml
    stages:
    - composability-test

    composability-test:
      image:
        name: grafana/k6:0.46.0-with-browser
        entrypoint: [""]
      stage: composability-test
      script:
        - echo "executing k6 composability test"
        - k6 run ./scripts/composability.js
    ```

- Commit the changes.

- Select on the left menu "Build" and "Jobs".

- Click on the job to view the results of running [scripts/composability.js](./scripts/composability.js) and review the output for the hybrid test:

  ![Browser test results](./images/composability-test-results-details.png)

## Stream test results to Grafana Cloud k6

There are two common ways to run k6 tests as part of a CI/CD process:

- k6 run to run a test on GitLab and [stream the results to Grafana Cloud k6](https://k6.io/docs/cloud/creating-and-running-a-test/cloud-tests-from-the-cli/#run-locally-and-stream-to-the-cloud).
- k6 cloud to run a test on Grafana Cloud k6. You will not be doing this in this workshop. See further instructions on https://k6.io/blog/integrating-load-testing-with-gitlab/#running-cloud-tests. 

In this step, you'll execute again the script [scripts/api.js](./scripts/api.js) and stream the results to Grafana Cloud k6:

- If you do not have an account with [Grafana Cloud](https://grafana.com/products/cloud/k6/), sign up for a [free account](https://grafana.com/auth/sign-up/create-user).
- Get your [k6 token](https://grafana.com/docs/grafana-cloud/k6/author-run/tokens-and-cli-authentication/).
  ![k6 token](./images/token.png)
- Note your Project ID, which is visible on the [project page](https://grafana.com/docs/grafana-cloud/k6/projects-and-users/projects/). In the image below, it would be `3650280`.
  ![Project ID](./images/projectID.png)
- Navigate to `Settings -> CI/CD` of your GitLab repository and expand variables.
- Create two variables: `K6_CLOUD_TOKEN` and `K6_CLOUD_PROJECT_ID`, with your token and project ID.
  - Ensure you uncheck the flag Protect variable when creating those variables.

    ![Create variable](./images/create-variable.png)
    ![Variable list](./images/variables.png)

- Go to **your forked repository** on gitlab.com and edit the file [`.gitlab-ci.yml`](.gitlab-ci.yml). Add the following content:

    ```yaml
    stages:
    - api-test-cloud

    api-test-cloud:
      image:
        name: grafana/k6:0.46.0
        entrypoint: [""]
      stage: api-test-cloud
      script:
        - echo "executing k6 api test and stream to cloud"
        # --out cloud will stream results to cloud
        - k6 run --out cloud ./scripts/api.js
    ```

- Select on GitLab's left menu "Build" and "Jobs".
- Click on the last job to view the results of running this test.

  ![Cloud test results](./images/cloud-output-test-results-details.png)

- Go back to Grafana Cloud k6 and wait for the test to finish.
  
  ![Cloud test](./images/cloud-test-list.png)

- Click on the test for details.

  ![Cloud test results](./images/cloud-test-results.png)
