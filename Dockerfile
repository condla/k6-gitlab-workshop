# Dockerfile to install chromium in the k6 image, to run k6 browser before version 0.46.0
FROM grafana/k6:0.45.1
USER root

RUN apk update && apk add --no-cache chromium

USER k6
ENV K6_BROWSER_ENABLED=true