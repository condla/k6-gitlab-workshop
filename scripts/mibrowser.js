import http from 'k6/http';
import { browser } from 'k6/experimental/browser';
import { sleep } from 'k6';

export const options = {
    thresholds: {
        //http_req_duration: ['p(95)<1000'], // 95 percent of response times must be below 1s
        browser_web_vital_lcp: ['p(90) < 400'],
    },
	scenarios: {
		ui: {
			executor: 'constant-vus',
			vus: 1,
			duration: '30s',
			options: {
				browser: {
					type: 'chromium',
				},
			},
		},
	},
};

export default async function () {
  const page = browser.newPage();
  try {
	await page.goto('http://grafana.datahovel.com:3389/shop?name=stefan#cats');
    await Promise.all([
        page.waitForNavigation(),
        page.locator('a[href="/cart?name=stefan"]').
        click(),
    ]);
  } finally {
    page.close();
  }
}
